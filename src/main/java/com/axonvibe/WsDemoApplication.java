package com.axonvibe;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@SpringBootApplication
@Controller
public class WsDemoApplication {
	private static final Logger logger = LoggerFactory.getLogger(WsDemoApplication.class);

	@RequestMapping("/")
	@ResponseBody
	String home() {

		int mb = 1024 * 1024;

		//Getting the runtime reference from system
		Runtime runtime = Runtime.getRuntime();

		String information = "";
		information += "##### Heap utilization statistics [MB] #####" + "\n";

		//Print used memory
		information += "Used Memory:"
				+ (runtime.totalMemory() - runtime.freeMemory()) / mb + "\n";

		//Print free memory
		information += "Free Memory:"
				+ runtime.freeMemory() / mb + "\n";

		//Print total available memory
		information += "Total Memory:" + runtime.totalMemory() / mb + "\n";

		//Print Maximum available memory
		information += "Max Memory:" + runtime.maxMemory() / mb + "\n";

		return information;
	}

	@MessageMapping("/gestures")
	@SendTo("/topic/gestures")
	public String gestureMessage(String message) throws Exception {
		// TODO filter data by: event, gesture before send to client
		logger.info("Receiver message: {}", message);
		return message;
	}

	@MessageMapping("/spectators")
	@SendTo("/topic/spectators")
	public String spectatorMessage(String message) throws Exception {
		// TODO filter data by: events before send to client
		logger.info("Receiver message: {}", message);
		return message;
	}

	public static void main(String[] args) {
		SpringApplication.run(WsDemoApplication.class, args);
	}
}
